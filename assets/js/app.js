(function() {
  'use strict';

  angular.module('application', [
    'ui.router',
    'ngAnimate',
    'application.controllers',
    'application.directives',
    'application.services',

    //foundation
    'foundation',
    'foundation.dynamicRouting',
    'foundation.dynamicRouting.animations'
  ])
    .config(config)
    .run(run)
  ;

  config.$inject = ['$urlRouterProvider', '$locationProvider'];

  function config($urlProvider, $locationProvider) {
    $urlProvider.otherwise('/');

    $locationProvider.html5Mode({
      enabled:false,
      requireBase: false
    });

    $locationProvider.hashPrefix('!');
  }

  function run() {
    FastClick.attach(document.body);
  }

  angular.module('application.controllers', []);
  angular.module('application.directives', []);
  angular.module('application.services', []);

})();

(function () {
  'use strict';

  angular.module('application.controllers')
    .controller('gameCtrl', [
    '$scope',
    '$state',
    '$http',
    function($scope, $state, $http){
      $scope.players = [
        {name: "Fred", points: 10},
        {name: "Player 1", points: 9},
        {name: "Player 2", points: 8},
        {name: "Player 3", points: 7},
        {name: "Player 4", points: 6},
        {name: "Player 5", points: 5},
        {name: "Player 6", points: 4},
        {name: "Player 7", points: 3},
        {name: "Player 8", points: 2},
        {name: "Player 9", points: 1},
      ];


      // Chat
      $scope.chat = [];
      $scope.color = "black";

      $scope.setColor = function(color){
        $scope.color = color;
      };

      $scope.submit = function() {
        if ($scope.text) {
          var currentUser = $scope.user.username || 'Fred';
          $scope.chat.push({user: currentUser, text: this.text});
          $scope.text = '';
        }
      };

      // Paint

    }]);
}());

(function () {
  'use strict';

  angular.module('application.controllers')
    .controller('mainCtrl', [
    '$scope',
    '$state',
    '$http',
    function($scope, $state, $http){

      $scope.user = {};

      $scope.signUp = function(){
        $state.go('home');
      };

      $scope.logIn = function(){
        $state.go('home');
      };

      $scope.logOut = function(){
        $state.go('/');
      };

      $scope.startGame = function(){
        $state.go('game');
      };

    }]);
}());

(function () {
  'use strict';

  angular.module('application.directives')
    .directive("drawing", function(){
      return {
        restrict: "A",
        link: function(scope, element){
          var ctx = element[0].getContext('2d');

          // variable that decides if something should be drawn on mousemove
          var drawing = false;

          // the last coordinates before the current move
          var lastX;
          var lastY;

          element.bind('mousedown', function(event){

            lastX = event.offsetX;
            lastY = event.offsetY;

            // begins new line
            ctx.beginPath();

            drawing = true;
          });
          element.bind('mousemove', function(event){
            if(drawing){
              // get current mouse position
              var currentX = event.offsetX;
              var currentY = event.offsetY;

              draw(lastX, lastY, currentX, currentY);

              // set current coordinates to last one
              lastX = currentX;
              lastY = currentY;
            }

          });
          element.bind('mouseup', function(event){
            // stop drawing
            drawing = false;
          });

          // canvas reset
          scope.resetDrawing = function(){
            ctx.clearRect(0, 0, canvas.width, canvas.height);
          };

          function draw(lX, lY, cX, cY){
            // line from
            ctx.moveTo(lX,lY);
            // to
            ctx.lineTo(cX,cY);
            // color
            ctx.strokeStyle = scope.color;
            // draw it
            ctx.stroke();
          }
        }
      };
    });

}());
